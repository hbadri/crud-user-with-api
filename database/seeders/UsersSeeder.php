<?php

namespace Database\Seeders;

use App\Models\Skill;
use App\Models\SkillUser;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = Skill::all()->toArray();
        $levels = ['Basic', 'Intermediate', 'Advance'];
        User::factory()->count(100)->create()->each(function($u) use($levels) {
            SkillUser::create([
                'skill_id' => 1,
                'user_id' =>$u->id,
                'level' => $levels[array_rand($levels)],
            ]);
        });
    }
}
